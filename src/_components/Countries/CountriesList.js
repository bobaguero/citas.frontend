import React from 'react';
import { connect } from "redux-zero/react";
import actions from "../../_actions/actions";

import empty from 'is-empty'

import TableList from '../../../_core/_ui/_components/TableList';
import TableHeader from '../../../_core/_ui/_components/TableHeader';
import Loader from '../../../_core/_ui/_components/Loader';

import { withStyles } from '@material-ui/core/styles';
import { AppStyles } from '../../_config/app'

import ApiService from '../../../_core/_backend/_services/Api.service';

import AddIcon from '@material-ui/icons/AddCircle';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/RemoveCircle';

class CountriesList extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
          params: ApiService.getDefaultParams()
        }
        
    }

    componentWillReceiveProps(nextProps){

      let { countries } = nextProps;

      if (!empty(countries.data)) {
          this.setState({
            countries
          })
      }

  }

    componentDidMount(){
      let { params } = this.state;
      this.props.LoadingActions({ action: 'change', params : true });
      this.props.CountryActions({ action: 'getAll', params });
    }

    deleteCountry(element, params){
      params.props.LoadingActions({ action: 'change', params : true });
      params.props.CountryActions({ action: 'delete', params: element.id });
      params.props.CountryActions({ action: 'getAll', params });
    }

    render() {
    
        const { countries } = this.props;

        console.log("CountriesList:this.props", this.props); 
        console.log("CountriesList:countries ",countries);

        let headers = ['Id','País']; // send headers
        let keys = ['id','name']; // send key
        let primaryKey = 'id';
        let actions = {
            'id' :  {
              url : {
                slug : "/country/view/$1",
                params: [ 'id' ]
              }
            },
            'Editar' :  {
              button : {
                icon: <EditIcon/>,
              },
              url : {
                slug : "/country/edit/$1",
                params: [ 'id' ]
              }
            },
            'Eliminar' : {
              button : {
                icon: <DeleteIcon/>,
              },
              url : {
                slug : "/country/delete/$1",
                params: [ 'id' ]
              },
              cb : {
                f : this.deleteCountry,
                params : {
                  props :this.props
                }
              }
            },
        };

        const listHeaderActions = [
          {
            type: 'button',
            icon : <AddIcon/>,
            label : 'Nuevo',
            cb : () => {
              this.props.history.push(`/country/add`)
            }
          }
        ]

        const data = !empty(countries.data) ? countries.data : {};
        
        return (
            <div className="List">

              <Loader loading={(countries.loading)} />
              
                <TableHeader  {...this.props} 
                              actions={listHeaderActions}
                              title='Listado de Paises' />

                <TableList  {...this.props}
                            data={data} 
                            headers={headers} 
                            keys={keys} 
                            actions={actions} 
                            primaryKey={primaryKey} /> 

            </div>

        );
    }
}

const mapToProps = ({  LoadingActions, countries, CountryActions }) => ({  LoadingActions, countries, CountryActions });
const connectedCountriesList = withStyles(AppStyles)(connect(mapToProps, actions)(CountriesList));
export { connectedCountriesList as CountriesList }; 
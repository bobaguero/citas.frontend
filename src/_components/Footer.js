
import React from 'react';
import AppConfig from '../_config/app'

class Footer extends React.Component {

  loadLinks() {

      let links = []

      for (var link in AppConfig.footer.links) {
        links.push(<p key={link}> <a href={AppConfig.footer.links[link].href} target="_blank">{AppConfig.footer.links[link].title}</a></p>);
      }

      return links;
      
  }

  render() {

      return (
        <div className="footer"> 
            {this.loadLinks()}
        </div>
      );
  }
  
}

export default Footer;
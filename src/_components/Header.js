import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { AppStyles } from '../_config/app'

import AppBar from '@material-ui/core/AppBar';

import IconButton from '@material-ui/core/IconButton';

import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Hidden from '@material-ui/core/Hidden';

import Logo from '../_images/Logo.png'

class Header extends React.Component {

  render(){

    const { classes, handleDrawerToggle } = this.props;

    return (
      <div className="header">
      
        <AppBar position="fixed" 
                color="default"
                className={classes.appBar} >

            <Toolbar disableGutters={true}>

              <Hidden smUp 
                      implementation="css">

                  <IconButton onClick={handleDrawerToggle}>
                    <MenuIcon />
                  </IconButton>
                  
              </Hidden>

              <div className="AlignCenter">
                <img  src={Logo} 
                      className="logo" />
              </div>
    
            </Toolbar>

        </AppBar>
        

      </div>
    );

  }
  
}

export default withStyles(AppStyles, { withTheme: true })(Header);
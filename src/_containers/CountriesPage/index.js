import React from 'react';

import { connect } from "redux-zero/react";
import actions from "../../_actions/actions";

// Core
import Utils from '../../../_core/_ui/_utils/Utils';

//Components
import ContentApp from '../../_components/ContentApp'
import { CountriesList } from '../../_components/Countries/CountriesList';
import { CountryAdd } from '../../_components/Countries/CountryAdd';
import { CountryEdit } from '../../_components/Countries/CountryEdit';

import '../../_css/App.css';

class CountriesPage extends React.Component {

    constructor(props) {
        super(props);
    }

    urls = {
        "^/countries" : CountriesList,
        "^/country/add" : CountryAdd,
        "^/country/edit/.*" : CountryEdit
    }
    
    render() {

        let TagName = Utils.getComponentByUrlList({
            urls        : this.urls,
            uri         : this.props.match.url
        })
        
        return (
            <ContentApp {...this.props} component={TagName} />   
        );

    }
}

const mapToProps = ({ user }) => ({ user });
const connectedCountriesPage = connect(mapToProps, actions)(CountriesPage);
export { connectedCountriesPage as CountriesPage };
import React from 'react';

import { connect } from "redux-zero/react";
import actions from "../../_actions/actions";
import empty from 'is-empty'

import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import '../../_css/App.css';

class HomePage extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const { user } = this.props;

        if (empty(user)){
            return '';
        }

        return (
            <Grid item xs={12} sm={6} className="ContentApp">
                <Paper>
                    <Typography component="p" color="textPrimary" className="FormHeader">
                        Bienvenid@ {user.firstName} {user.lastName}
                    </Typography>
                </Paper>
            </Grid>
        );
    }
}

const mapToProps = ({ 
    user
}) => ({
    user
});
const connectedHomePage = connect(mapToProps, actions)(HomePage);
export { connectedHomePage as HomePage }; 
const DEFAULT = {
    loading: false,
    country: {},
    countries: {},
    alert: {},
    user: {}
} 
export default DEFAULT;
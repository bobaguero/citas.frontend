const AppConfig = {
    header : {
        drawerWidth: 260
    },
    theme : {
        typography: {
            useNextVariants: true,
        },
        palette: {
          primary: {
              main:'#5f3d08',
          },
          secondary: { 
              main: '#e62783' 
          },
          text:{
              primary: '#5f3d08',
              secondary: '#6d6d6d'
          }
        },
    },
    footer : {
        links: [
            {
                href: 'https://spamanos.com/',
                title: 'Spa Manos',
            },
            {
                href: 'https://spamanosvenezuela.com/',
                title: 'Spa Manos - Venezuela',
            }
        ]
    }
}

export const AppStyles = theme => {

    let drawerWidth = AppConfig.header.drawerWidth

    return {
        root: {
          display: 'flex',
        },
        appBar: {
            marginLeft: drawerWidth,
            [theme.breakpoints.up('sm')]: {
              width: `calc(100% - ${drawerWidth}px)`,
            },
          },
        drawer: {
            [theme.breakpoints.up('sm')]: {
                width: drawerWidth,
                flexShrink: 0,
            },
        },
        list:{
            width: drawerWidth,
        },
        drawerPaper: {
          width: drawerWidth,
        },
        content: {
          flexGrow: 1,
          padding: theme.spacing.unit * 3,
          [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
            marginTop: "65px"
          },
        },
        toolbar: {
            [theme.breakpoints.down('sm')]: {
                marginTop: "65px"
            },
        },
    }

}

export default AppConfig;


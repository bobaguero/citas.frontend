import AlertActions from './Alert.actions';
import AuthService  from '../_services/Auth.service';
import { history } from '../../_core/_helpers/history';
import empty from 'is-empty';

class AuthActions {

    static handler(state, params){
        console.log("AuthActions::handler::", "State:", state, "Params", params)
        return new AuthActions[params.action](state, params.params);
    }

    static logout(state){
        AuthService.logout();
        return { user: {} };
    }

    static isLogged(state){
        let user = JSON.parse(localStorage.getItem('user'))
        return {
            user : (empty(user)) ? {} : user
        }
    }

    static async login(state, {username, password} = {}) {

        try { 
            let user =  await AuthService.login({username, password})
            localStorage.setItem('user', JSON.stringify(user));
            history.push("/");
            return  {
                loading: false, 
                user 
            }
            
        } catch (error) {
            console.log("Error:Login", error)
            return AlertActions.error(error.message)
        }
        
    }

    static async register(state, userData){

        try { 
            let user =  await AuthService.register(userData)
            history.push("/login");
            return  {
                loading: false,
                alert: AlertActions.create('success','Usuario guardado con exito'),
                user 
            }
            
        } catch (error) {
            console.log("Error:Register", error)
            return AlertActions.error(error.message)
        }
    }

} 

export default AuthActions;
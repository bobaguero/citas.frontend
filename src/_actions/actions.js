import AuthActions from "./Auth.actions"
import LoadingActions from "./Loading.actions"
import CountryActions from "./Country.actions"

const actions = store => ({
    LoadingActions : LoadingActions.handler,
    CountryActions : CountryActions.handler,
    AuthActions : AuthActions.handler
});

export default actions;
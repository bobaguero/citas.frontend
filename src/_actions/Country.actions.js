import AlertActions from './Alert.actions';
import CountryService  from '../_services/Country.service';

class CountryActions {

    static handler(state, params){
        console.log("CountryActions::handler::", "State:", state, "Params", params)
        return new CountryActions[params.action](state, params.params);
    }

    static async getById(state, id ) {

        try { 
            let result =  await CountryService.getById(id)
            return  {
                loading: false, 
                country: result
            }
            
        } catch (error) {
            console.log("Error:CountryActions:getById", error)
            return AlertActions.error(error.message)
        }
    }

    static async getAll( state, params ) {

        try { 
            let result =  await CountryService.getAll(params)
            console.log('CountryActions::getAll:result', result);
            return  {
                loading: false,
                countries : {
                    data : result
                }
            }
            
        } catch (error) {
            console.log("Error:CountryActions:getAll", error)
            return AlertActions.error(error.message)
        }
    }

    static async add(state, country){

        console.log('country',country);

        try { 
            let result =  await CountryService.add(country)
            console.log('CountryActions::add:result', result);
            return  {
                loading: false,
                alert: AlertActions.create('success','Pais guardado con exito')
            }
            
        } catch (error) {
            console.log("Error:CountryActions:add", error)
            return AlertActions.error(error.message)
        }

    }

    static async edit(state, country){

        console.log('CountryActions:edit:country',country);

        try { 
            let result =  await CountryService.edit(country)
            console.log('CountryActions:edit', result);
            return  {
                loading: false,
                country: result,
                alert: AlertActions.create('success','Pais actualizado con exito')
            }
            
        } catch (error) {
            console.log("Error:CountryActions:edit", error)
            return AlertActions.error(error.message)
        }

    }

    static async delete(state, id){

        try { 
            let result =  await CountryService.delete(id)
            console.log('CountryActions::add:delete', result);
            return  {
                loading: false,
                alert: AlertActions.create('success','Pais eliminado con exito')
            }
            
        } catch (error) {
            console.log("Error:CountryActions:delete", error)
            return AlertActions.error(error.message)
        }

    }

} 

export default CountryActions;
class AlertActions {

    static create(type, message){
        return {
            type,
            message
        }
    }
    static success(message) {
        return { 
            loading: false,
            alert: {
                type: 'success',
                message
            } 
        };
    }
    
    static error(message) {
        return { 
            loading: false,
            alert: {
                type: 'error',
                message
            } 
        };
    }
    
    static clear() {
        return { 
            loading: false,
            alert:{} 
        };
    }

}

export default AlertActions;


class LoadingActions {

    static handler(state, params){
        console.log("LoadingActions::handler::", "State:", state, "Params", params)
        return new LoadingActions[params.action](state, params.params);
    }

    static change(state, value) {
        return { 
            alert : {},
            loading: (value == true) ? true : false
        };
    }

}

export default LoadingActions;


import React from 'react';
import empty from 'is-empty'
import { Router, Route } from 'react-router-dom';
import { connect } from "redux-zero/react";
import actions from "../_actions/actions";

import { withStyles } from '@material-ui/core/styles';
import { AppStyles } from '../_config/app'

import { history } from '../../_core/_helpers/history';
import { PrivateRoute } from '../_components/PrivateRoute';
import { PrivateRouteAdmin } from '../_components/PrivateRouteAdmin';
import Grid from '@material-ui/core/Grid';
import '../_css/App.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import AlertNotification from '../../_core/_ui/_components/AlertNotification' 

import AppConfig from '../_config/app'
import Header from '../_components/Header';
import { Menu } from '../_components/Menu'
import Footer from '../_components/Footer';

import { HomePage } from '../_containers/HomePage';
import { LoginPage } from '../_containers/LoginPage';
import { RegisterPage } from '../_containers/RegisterPage';
import { DashboardPage } from '../_containers/DashboardPage';
import { CountriesPage } from '../_containers/CountriesPage';
import Loader from '../../_core/_ui/_components/Loader';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }
    
    handleDrawerToggle = () => {
        this.setState({ open: !this.state.open });
    };

    componentDidMount(){
        this.props.AuthActions({
            action : 'isLogged'
        });
    }

    render() {

        const { classes, alert, loading } = this.props;

        console.log("App:Index:alert", alert);

        return (
            <MuiThemeProvider theme={createMuiTheme(AppConfig.theme)}>
            
                    <Router history={history}>

                        <div>

                            <Loader loading={loading} />
                            
                            <Header handleDrawerToggle={this.handleDrawerToggle} />

                            <Menu {...this.state} history={history} handleDrawerToggle={this.handleDrawerToggle} />

                            <Grid item xs={12} className={classes.content}>

                            <div className={classes.toolbar} />

                            {
                                (!empty(alert.message)) ? 
                                    <AlertNotification {...alert} />
                                : ''
                            }

                            <PrivateRoute exact path="/" component={HomePage} />
                            <PrivateRoute exact path="/dashboard" component={DashboardPage} />
                            <PrivateRouteAdmin exact path="/countries" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/add" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/view/:id" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/edit/:id" component={CountriesPage} />
                            <PrivateRouteAdmin path="/country/delete/:id" component={CountriesPage} />
                            <Route path="/login" component={LoginPage} />
                            <Route path="/register" component={RegisterPage} />

                            <Footer />

                            </Grid>

                            

                        </div>

                        

                    </Router>

                
                
            </MuiThemeProvider>
        );
    }
}

const mapToProps = ({ alert, AuthActions, loading }) => ({ alert, AuthActions, loading });
const connectedLoginPage = withStyles(AppStyles, { withTheme: true })(connect(mapToProps, actions)(App));
export { connectedLoginPage as App }; 
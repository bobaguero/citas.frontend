import config from '../../../src/_config/services.config';
import empty from 'is-empty'
import queryString from 'query-string'

class ApiService {

    static authHeader() {
        // return authorization header with jwt token
        let user = JSON.parse(localStorage.getItem('user'));
    
        if (user && user.token) {
            return { 'Authorization': 'Bearer ' + user.token };
        } else {
            return {};
        }
    }

    static getDefaultParams () {

        let filters = {
            and: {},
            or: {}
        };
        let page = {
            limit : 10,
            number: 1
        }
        let params = {
            page: JSON.stringify(page),
            filters: JSON.stringify(filters)
        };
        return params;
    }

    static handlerFetch( { url, requestOptions }) {

        return fetch(`${config.apiUrl}${url}`, requestOptions)
        .then(ApiService.handleResponse)

    }

    static handlerFetchPost({ url, data } = {}) {

        let requestOptions = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                ...ApiService.authHeader()
            },
            body: JSON.stringify(data)
        };

        console.log('handlerFetchPost:url',url);
        console.log('handlerFetchPost:data',data);
        console.log('handlerFetchPost:requestOptions',requestOptions);

        return ApiService.handlerFetch({
            url,
            requestOptions
        })
        
    }

    static handlerFetchPut({ url, data } = {}) {

        let requestOptions = {
            method: 'PUT',
            headers: { 
                'Content-Type': 'application/json',
                ...ApiService.authHeader()
            },
            body: JSON.stringify(data)
        };

        console.log('handlerFetchPost:url',url);
        console.log('handlerFetchPost:data',data);
        console.log('handlerFetchPost:requestOptions',requestOptions);

        return ApiService.handlerFetch({
            url,
            requestOptions
        })
        
    }

    static handlerFetchDelete({ url, data } = {}) {

        let requestOptions = {
            method: 'DELETE',
            headers: { 
                'Content-Type': 'application/json',
                ...ApiService.authHeader()
            },
            body: JSON.stringify(data)
        };

        console.log('handlerFetchPost:url',url);
        console.log('handlerFetchPost:data',data);
        console.log('handlerFetchPost:requestOptions',requestOptions);

        return ApiService.handlerFetch({
            url,
            requestOptions
        })
        
    }

    static handlerFetchGet({ url, params } = {}) {

        let requestOptions = {
            method: 'GET',
            headers: ApiService.authHeader()
        };

        if (!empty(params)){
            url += '?'+ queryString.stringify(params, {arrayFormat: 'index'})
        }

        return ApiService.handlerFetch({
            url,
            requestOptions
        })

    }

    static handleResponse( response ) {

        return response.text().then(text => {
            
            const data = text && JSON.parse(text);
            console.log("SERVICE handleResponse::text", text);
            console.log("SERVICE handleResponse::data", data);
            if (!response.ok) {
                if (response.status === 401) {
                    AuthService.logout();
                    location.reload(true);
                }
    
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
            

            if (!empty(data.data)){
                return data.data;
            }

            return data;
    
            
        });
        
    }
}

export default ApiService;
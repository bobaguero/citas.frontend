import { firebaseSettings, firebaseConfig } from '../../../src/_config/firebase.config';
import empty from 'is-empty'
import queryString from 'query-string'

import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';


firebase.initializeApp(firebaseConfig);
firebase.firestore().settings(firebaseSettings);

class FirebaseService {

    static authRef = firebase.auth();
    static database = firebase.firestore();
    static provider = new firebase.auth.GoogleAuthProvider();

    static async add({ obj, data, uid } = {}){

        try {
            if (!empty(uid)) {
                return await FirebaseService.database.collection(obj).doc(uid).set(data);
            } else {
                return await FirebaseService.database.collection(obj).add(data);
            }
            
        } catch(error) {
            console.log('Error:FirebaseService::add ', error);
            throw new Error('Error:FirebaseService::add no se logro agregar correctamente');
        }
        
    }

    static async update({ obj, data, uid } = {}){

        try {

            return await FirebaseService.database.collection(obj).doc(uid).update(data);
            
        } catch(error) {
            console.log('Error:FirebaseService::update ', error);
            throw new Error('Error:FirebaseService::update no se logro actualizar correctamente');
        }
        
    }

    static async delete({ obj, uid } = {}){

        try {
            return await FirebaseService.database.collection(obj).doc(uid).delete();
        } catch(error) {
            console.log('Error:FirebaseService::update ', error);
            throw new Error('Error:FirebaseService::update no se logro actualizar correctamente');
        }
        
    }

    static async getById({obj, uid } = {}){

        try {
            let data={};
            const dataById = await FirebaseService.database.collection(obj).doc(uid).get();

            if (dataById.exists) {
                data = dataById.data();
                data.id = uid;
            }

            return data;
            
        } catch(error) {
            console.log('Error:FirebaseService::getById ', error);
            throw new Error('Error:FirebaseService::getById no se ejecuto bien');
        }

    }

    static async getAll({ obj, limit, conditions } = {}){

        try {
            let l = (empty(limit)) ? 10 : limit;
            let result = {}; 
            result.data = []; 
            let ref =  FirebaseService.database.collection(obj);
            //ref = ref.where("name", "==" ,"Venezuela");
            ref = ref.limit(l);
            let data = await ref.get();
            data.forEach(doc => {
                let row = [];
                row = doc.data();
                row.id = doc.id;
                result.data.push(row);
            });
            return result;
        } catch(error) {
            console.log('Error:FirebaseService::getAll ', error);
            throw new Error('Error:FirebaseService::getAll no se ejecuto bien');
        }

    }

}

export default FirebaseService;
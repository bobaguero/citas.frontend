/**
 * Clase de utilidad para los eventos claves de los elementos de los formularios
 * autor: Andres Rosales
 * fecha: 20/12/2018
 */
import empty from 'is-empty'

class FormsUtils {

    static isAFunction(fun){
        return typeof fun === "function";
    }

    static handleBlur(e, context) {
        const { name } = e.target;
        console.log("handleBlur",e);
        context.setState({
          touched: { ...context.state.touched, [name]: true },
        }, () => {
            if (FormsUtils.isAFunction(context.validate)) {
                context.validate()
            }
        });
    }

    static handleChange({e, context, target} = {}) {
        const { name, value } = e.target;
        let changevalue = {};

        if( !empty(target) ) {
            changevalue[target] = context.state[target];
            changevalue[target][name] = value;
        } else {
            changevalue[name] = value;
        }

        context.setState({ 
            changevalue,
            touched: { ...context.state.touched, [name]: true }
        }, () => {
            if (FormsUtils.isAFunction(context.validate)) {
                context.validate()
            }
        });

        
    }

    static shouldBeSubmit(context){
        const { errors } = context.state;
        context.setState({
            activedButtonSubmit : (empty(errors) && FormsUtils.handleTouched(context))
        })
    }

    static handleTouched(context){
        const { touched } = context.state;
        let result = true
        for (let element in touched){
            if(touched[element] == false){
                result = false;
            }
        }
        return result;
    }

}

export default FormsUtils;
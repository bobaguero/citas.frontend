import React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

class TableHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    getButton(key, action){

        let { icon, label, cb } = action;

        return (
            <Button key={'button_'+key} 
                    onClick={cb}  
                    color='default'>
                    {icon} &nbsp; {label} 
            </Button>
        )
    }

    render() {
        const { title, actions } = this.props;

        let rightActions = [];
        for (let key in actions) {
            //console.log(actions[key]);
            let a = actions[key];
            if (a.type == 'button') {
                rightActions.push(this.getButton(key,a));
            }

        }

        return (
            <Grid container item xs={12} className="ListHeader">

                <Grid   item
                        className="ListTitle" 
                        xs={6} >

                    <Typography variant="h5" 
                                component="h3" 
                                color="textPrimary">  
                        {title}
                    </Typography>
                    
                </Grid>

                <Grid   item 
                        className="ListMenu"
                        xs={6} >

                    {rightActions}

                </Grid>

                
            </Grid>
        );
    }

}


export default TableHeader;
import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class AlertNotification extends React.Component {

    constructor(props) {
        super(props);
    }

    showToast(type, message){

        const options = {
            hideProgressBar: true,
            pauseOnHover: true,
            position: toast.POSITION.TOP_CENTER
        };

        if (type == 'success'){
            toast.success(message, options );
        }

        if (type == 'error'){
            toast.error(message, options);
        }

    }

    componentDidMount(){
        console.log("Trigger:ALERT",this.props);
        const { message, type } = this.props;
        this.showToast(type, message);
    }

    componentWillReceiveProps(nextProps){
        console.log("AlertNotification:componentWillReceiveProps",nextProps);
        const { message, type } = nextProps;
        this.showToast(type, message);
    }

    render() {
        return (
            <div>
                <ToastContainer />
            </div>
        );
    }

}

export default AlertNotification;
/**
 * Componente para los DropDown
 */
import React from 'react';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { func } from 'prop-types';
import empty from 'is-empty'

class DropDown extends React.Component {

    constructor(props) {
        super(props);
    }

    build() {

        let optionItems = [],
            items = this.props.data;
            for (var item in items) {
                optionItems.push(
                    <MenuItem   key={items[item][this.props.keyD]} 
                                value={items[item][this.props.keyD]} >
                        {items[item][this.props.value]}
                    </MenuItem>
                );
            }

        return optionItems;

    }

    render() {

        let valueSelect = (this.props.selected)? this.props.selected : '';
        return (
                <Grid   item 
                        xs={12} >
                    <FormControl    className="FormControlSelect" 
                                    {...((!empty(this.props.error)) && { error: true })} >

                        <InputLabel htmlFor="typeusers"> 
                            {this.props.label}
                        </InputLabel>

                        <Select
                            onChange={this.props.handleChange}
                            name={this.props.name}
                            value={valueSelect}
                        >
                            <MenuItem>Seleccione...</MenuItem>
                            {this.build()}
                        </Select>
                        
                    </FormControl>
                </Grid>
        );

    }
}

DropDown.propTypes = {
    handleChange: func.isRequired,
};

export default DropDown;
import { getCountries, getUsers } from '../_helpers/fake-data';
import empty from 'is-empty'
import queryString from 'query-string'


function getLocation(href) {
    var location = document.createElement("a");
    location.href = href;
    if (location.host == "") {
      location.href = location.href;
    }

    // location.protocol; // => "http:"
    // location.host;     // => "example.com:3000"
    // location.hostname; // => "example.com"
    // location.port;     // => "3000"
    // location.pathname; // => "/pathname/"
    // location.hash;     // => "#hash"
    // location.search;   // => "?search=test"
    // location.origin;   // => "http://example.com:3000"
    return location;
};

function parseParams(params) {

    return {
        page : JSON.parse(params.page),
        filters : JSON.parse(params.filters)
    }; 
}

function getAllDateWithFilters(params, data){

    let result = {};
    let offset = 0;
    let size = 10;

    if(!empty(params.page)){
        offset = (params.page.number - 1 ) * params.page.limit;
        size = (params.page.number) * params.page.limit;
    }

    if ( (!empty(params.filters)) && !empty(params.filters.and) ){

        for (const [key, value] of Object.entries(params.filters.and)) { 

            data = data.filter(function(itm){
                return itm[key] == value;
            });
                
        }
    
    }
    
    result.total = data.length;
    result.data  = data.slice(offset, size)
    return result;
}


export function configureFakeBackend() {
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        //console.log(url,opts);
        let urlObj = getLocation(url);
        let urlPath = urlObj.pathname;
        let params = !empty(urlObj.search) ? queryString.parse(urlObj.search) : {};
        params = (!empty(params)) ? parseParams(params) : params;
        
        console.log("Full URL", url);
        console.log("urlPath", urlPath);
        console.log("Query params", params);
        

        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(() => {

                // authenticate
                if (urlPath.endsWith('/users/authenticate') && opts.method === 'POST') {
                    // get parameters from post request
                    let params = JSON.parse(opts.body);
                    let users = getUsers();

                    // find if any user matches login credentials
                    let filteredUsers = users.filter(user => {
                        return user.username === params.username && user.password === params.password;
                    });

                    if (filteredUsers.length) {
                        // if login details are valid return user details and fake jwt token
                        let user = filteredUsers[0];
                        let responseJson = {
                            id: user.id,
                            firstName: user.firstName,
                            lastName: user.lastName,
                            birthday: user.birthday,
                            phone: user.phone,
                            username: user.username,
                            companyId: user.companyId,
                            type: user.type,
                            status: user.status,
                            token: 'fake-jwt-token'
                        };

                        let result = [];
                        result = {
                            data: responseJson
                        }

                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(result)) });
                    } else {
                        // else return error
                        reject('Usuario / Contraseña son invalidos');
                    }

                    return;
                }

                // get users
                if (urlPath.endsWith('/users') && opts.method === 'GET') {
                    // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
                        let users = getUsers();
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(users))});
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorised');
                    }

                    return;
                }

                // get user by id
                if (urlPath.match(/\/users\/\d+$/) && opts.method === 'GET') {
                    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
                        // find user by id in users array
                        let urlParts = url.split('/');
                        let id = parseInt(urlParts[urlParts.length - 1]);
                        let users = getUsers();

                        let matchedUsers = users.filter(user => { return user.id === id; });
                        let user = matchedUsers.length ? matchedUsers[0] : null;

                        // respond 200 OK with user
                        resolve({ ok: true, text: () => JSON.stringify(user)});
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorised');
                    }

                    return;
                }

                // register user
                if (urlPath.endsWith('/users/register') && opts.method === 'POST') {
                    // get new user object from post body
                    let rawData = JSON.parse(opts.body);
                    let newUser = rawData.user;
                    let result = [];
                    let users = getUsers();

                    // validation
                    let duplicateUser = users.filter(user => { return user.username === newUser.username; }).length;
                    if (duplicateUser) {
                        reject('Username "' + newUser.username + '" is already taken');
                        return;
                    }

                    // save new user
                    newUser.id = users.length ? Math.max(...users.map(user => user.id)) + 1 : 1;
                    users.push(newUser);
                    localStorage.setItem('users', JSON.stringify(users));

                    result = {
                        data : newUser
                    }

                    // respond 200 OK
                    resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(result)) });

                    return;
                }

                // get countries
                if (urlPath.endsWith('/countries') && opts.method === 'GET') {
                    // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {

                        let result = [];
                        let data = getAllDateWithFilters(params,getCountries());
                        result = {data}

                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(result))});
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorised');
                    }

                    return;
                }

                // addcountry
                if (urlPath.endsWith('/countries/add') && opts.method === 'POST') {

                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {

                        // get new Country object from post body
                        let rawData = JSON.parse(opts.body);
                        let newCountry = rawData.country;

                        console.log("newCountry",newCountry);

                        // validation
                        let countries = getCountries();
                        let duplicateCountry = countries.filter(country => { return country.name === newCountry.name; }).length;
                        if (duplicateCountry) {
                            reject('Country "' + newCountry.name + '" is already taken');
                            return;
                        }

                        console.log(countries);
                        // save new Country
                        newCountry.id = countries.length ? Math.max(...countries.map(country => country.id)) + 1 : 1;
                        countries.push(newCountry);
                        localStorage.setItem('countries', JSON.stringify(countries));

                        let result = [];
                        result = {
                            data : newCountry
                        }

                        // respond 200 OK
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(result)) });

                        return;

                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorized :: Countries Edit');
                    }
                }

                // get country by id
                if (urlPath.match('/countries/edit/[0-9]{1,99}') && opts.method === 'PUT') {
                    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
                        // find user by id in users array

                        // get new Country object from post body
                        let rawData = JSON.parse(opts.body);
                        let dataToUpdate = rawData.country;

                        let result = [];
                        let urlParts = urlPath.split('/');
                        let id = parseInt(urlParts[urlParts.length - 1]);
                        let countries = getCountries();

                        console.log("Edit:CurrentCountries After",countries);
                        countries[countries.findIndex(el => el.id === dataToUpdate.id)] = dataToUpdate;
                        console.log("Edit:CurrentCountries Before",countries);

                        localStorage.setItem('countries', JSON.stringify(countries));

                        let matchedCountries = countries.filter(country => { return country.id === id; });
                        let country = matchedCountries.length ? matchedCountries[0] : null;

                        result = {
                            data: country
                        }

                        console.log("result",result);

                        // respond 200 OK with user
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(result))});
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorized :: Countries Edit');
                    }

                    return;
                }


                // get country by id
                if (urlPath.match('/countries/delete/[0-9]{1,99}') && opts.method === 'DELETE') {
                    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
                        // find user by id in users array

                        // get new Country object from post body
                        let rawData = JSON.parse(opts.body);
                        let id = rawData.id;

                        let result = [];
                        let countries = getCountries();
                        
                        console.log("Delete:CurrentCountries After",countries);

                        var index = countries.findIndex(function(o){
                            return o.id === id;
                        })
                        if (index !== -1) countries.splice(index, 1);


                        console.log("Delete:CurrentCountries Before",countries);
                        localStorage.setItem('countries', JSON.stringify(countries));

                        let data = getAllDateWithFilters({},getCountries());

                        result = {
                            data: data
                        }

                        // respond 200 OK with user
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(result))});
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorized :: Countries Edit');
                    }

                    return;
                }

                // get country by id
                if (urlPath.match(/\/countries\/\d+$/) && opts.method === 'GET') {
                    // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
                    if (opts.headers && opts.headers.Authorization === 'Bearer fake-jwt-token') {
                        // find user by id in users array

                        let result = [];
                        let urlParts = urlPath.split('/');
                        let id = parseInt(urlParts[urlParts.length - 1]);
                        let countries = getCountries();

                        let matchedCountries = countries.filter(country => { return country.id === id; });
                        let country = matchedCountries.length ? matchedCountries[0] : null;

                        result = {
                            data: country
                        }

                        console.log("result",result);

                        // respond 200 OK with user
                        resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(result))});
                    } else {
                        // return 401 not authorised if token is null or invalid
                        reject('Unauthorized');
                    }

                    return;
                }


                // pass through any requests not handled above
                realFetch(url, opts).then(response => resolve(response));

            }, 800);
        });
    }
}
import moment from 'moment';
import empty from 'is-empty';

export function setTest(num)
{
    let a = 2 + num;
    return a;
}

export function getTypeUsers()
{
    return([
        {id:1,description:"Administrador",status:"active"},
        {id:2,description:"Supervisor",status:"active"},
        {id:3,description:"Cliente",status:"active"}]
    );
}

export function getUsers(){
    return JSON.parse(localStorage.getItem('users')) || [];
}

export function getCountries()
{
    let countries=[];

    if(!empty(localStorage.getItem('countries'))){
        countries = JSON.parse(localStorage.getItem('countries'));
    }else{
        countries = [
            {id:1,name:"Venezuela"},
            {id:2,name:"Panamá"},
            {id:3,name:"Estados Unidos"}
        ];
        localStorage.setItem('countries', JSON.stringify(countries));
    }
    return(countries);
}


export function getTowns()
{
return (
    {
        'Anzoátegui':['SpaManos Plaza Mayor','SpaManos Puerto La Cruz'],
        'Aragua':['SpaManos Torre Imperial'],
        'Bolívar':['SpaManos Babilonia','SpaManos Orinokia','SpaManos C.C. Atlántico'],
        'Carabobo':['SpaManos Concepto La Viña','SpaManos Naguanagua','SpaManos La Trigaleña','SpaManos Guataparo','SpaManos San Diego'],
        'Distrito Capital':['SpaManos Santa Mónica','SpaManos IPSFA','SpaManos CC Bera – La Candelaria',
          'SpaManos CC Perico – La Candelaria'],
        'Falcón': ['SpaManos Costa Azul','SpaManos Paraguaná'],
        'Gran Caracas':['SpaManos Centro Plaza','SpaManos C.C. Líder','SpaManos C.C. Millennium','SpaManos Macaracuay',
        'SpaManos Chacao','SpaManos Galerías El Recreo','SpaManos CC Concresa','SpaManos Manzanares',
         'SpaManos Expreso La Trinidad','SpaManos Centro Letonia','SpaManos La Boyera','SpaManos Licarch','SpaManos CC Paseo Las Mercedes',
         'SpaManos Centro San Ignacio','SpaManos La Tahona','SpaManos Los Samanes',
         'SpaManos Centro Comercial Vizcaya','SpaManos Expreso Baruta. Ref (EPA)','SpaManos Centro Comercial Santa Fe',
         'SpaManos Parque Comercial El Avila','SpaManos Centro Comercial Terrazas de la Lagunita',
         'SpaManos Centro Comercial Galerías Los Naranjos','SpaManos Centro Comercial Galerías Prados del Este',
         'SpaManos Santa Rosa de Lima','SpaManos Terras Plaza','SpaManos CC Plaza Las Américas I',
         'SpaManos Centro Comercial Paseo El Hatillo','SpaManos Valle Arriba','SpaManos Sebucán'],
        'Lara': ['SpaManos Libertador','SpaManos Trinitarias'],
        'Miranda':['SpaManos La Casona','SpaManos CC Buenaventura'],
        'Monagas':['SpaManos Maturín'],
        'Nueva Esparta':['SpaManos Margarita','SpaManos La Isla'],
        'Sucre':['SpaManos Centro Gina'],
        'Táchira':['SpaManos San Cristóbal'],
        'Trujillo':['SpaManos Valera'],
        'Vargas':['SpaManos Los Corales','SpaManos Playa Grande'],
        'Ciudad de Panamá':['Albrook','AltaPlaza','Calle 50','Costa del Este','Metromall','MultiPlaza','Westland']
        
    }
);

}

export function getEmployees(){
    return({
        'SpaManos Centro Plaza':['Jessika Peñalver','Ana Anuel','Barbara Becker','Carolina Castillo','Desiree DeFreitas']
      });
}

export function getServices(){
    return({
        'Jessika Peñalver':['Manicure','Pedicure','Sistemas']
      });
}

export function getEmployeesDateAvails(){
    return({
        'Jessika Peñalver':['10/12/2018','11/12/2018','14/12/2018','15/12/2018']
      });
}

export function getAvails()
{
return ({
    '10/12/2018' : ['8:00 am', '10:00 am', '1:00 pm'],
    '11/12/2018': ['8:00 am', '9:00 am', '10:00 am','11:00 am'],
    '12/12/2018': ['8:00 am', '9:00 am', '10:00 am','11:00 am'],
    '13/12/2018': ['8:00 am', '9:00 am', '10:00 am','11:00 am'],
    '14/12/2018': ['8:00 am', '9:00 am', '10:00 am','11:00 am'],
    '15/12/2018': ['8:00 am', '9:00 am', '10:00 am','11:00 am'],
    '16/12/2018': ['8:00 am', '9:00 am', '10:00 am','11:00 am'],
  });
}

export function getMonths()
{
    return([{value: '1', label: '1'},{value: '2',label: '2'},{value: '3',label: '3'},{value: '4',label: '4'},
    {value: '5',label: '5'},{value: '6',label: '6'},{value: '7',label: '7'},{value: '8',label: '8'},
    {value: '9',label: '9'},{value: '10',label: '10'},{value: '11',label: '11'},{value: '12',label: '12'}]);
}
export function getYears()
{
    return([{value: '2018', label: '2018'},{value: '2019',label: '2019'},{value: '2020',label: '2020'},{value: '2021',label: '2021'},
    {value: '2022',label: '2022'},{value: '2023',label: '2023'},{value: '2024',label: '2024'},{value: '2025',label: '2025'}]);
}

export function getBanks()
{
    return([{value: 'Banesco', label: 'Banesco'},{value: 'Banco de Venezuela',label: 'Banco de Venezuela'},{value: 'Banco Provincial',label: 'Banco Provincial'},{value: 'Banco Mercantil',label: 'Banco Mercantil'}]);
}

export function getEvents()
{
    let today = moment();
    return ([
        {
            id: 0,
            title: 'Manicure de Victoria Von',
            start: moment(today),
            end: moment(today),
        },
        {
            id: 1,
            title: 'Pedicure de Maria Mendoza',
            start: moment(today),
            end: moment(today),
        },
        {
            id: 2,
            title: 'Pedicure de Nathy Nole',
            start: moment(today).add(1, 'days'),
            end: moment(today).add(1, 'days'),
        },
        ,
        {
            id: 3,
            title: 'Manicure de Rosa Ramos',
            start: moment(today).add(4, 'days'),
            end: moment(today).add(4, 'days'),
        }
      ]);
}